# TeamJaune Documentation
This project uses OpenAPI + ReactJS + Stoplight to serve documentation

# Writing Documentation
Use Stoplight Studio to write the documentation or modeify the JSON manually
you can download it here : https://stoplight.io/studio/

# Run the documentation
```
git clone https://gitlab.com/teamjaune/documentation.git
cd docuementation
yarn install
yarn start
```

# Building the documentation
```
git clone https://gitlab.com/teamjaune/documentation.git
cd docuementation
yarn install
yarn build
```